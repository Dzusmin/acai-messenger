# acai-messenger

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Project Uses](#project-uses)
* [Setup](#setup)
* [Tests](#tests)

## General info
This project is basic message storage written for job application.


Routes implemented:

* POST   /api/messages
    * example request body:
    ``` json
    {
        "title": "example title",
        "content": "example content",
        "email": "example@example.co"
    }
    ```
    * example response body
    ``` json
    {
        "id": 12,
        "code": "000000",
        "title": "example title",
        "content": "example content",
        "email": "example@example.co"
    }
    ```
* DELETE /api/messages/:id
    * example request:
    ```
    /api/messages?email=example@example.co&page=1&limit=150
    ```
    ``` json
        {
            "code": "000000"
        }
    ```

    * example response
        * code: 200 and empty body
* GET    /api/messages
    * example request:
    ```
    /api/messages?email=example@example.co&page=1&limit=150
    ```
    * example response body
    ``` json
    [
        {
            "id": 12,
            "code": "000000",
            "title": "example title",
            "content": "example content",
            "email": "example@example.co"
        },
        {
            "id": 14,
            "code": "000000",
            "title": "example title",
            "content": "example content",
            "email": "example@example.co"
        }
        ...
    ]
    ```

## Technologies
Project is created with:
* Go: 1.16.2
* Docker: 20.10.5

## Project uses
Project on local environment also uses:
* MySQL: 8.0
	
## Setup
To run this project, clone this repository:

``` bash
$ git clone https://bitbucket.org/Dzusmin/acai-messenger.git
```

Next, start-up this project:
``` bash
$ make up
```

## Tests

There are only unit tests in this project for now.

To tun tests for this project:

``` bash
$ make tests
```

To check linter:

``` bash
$ make lint
```