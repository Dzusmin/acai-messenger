package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func ConnectDataBase() (*gorm.DB, error) {

	dsn := "root:root@tcp(acai-messenger-database:3306)/acai-messenger?charset=utf8mb4&parseTime=True&loc=Local"
	database, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	err = database.AutoMigrate(&Message{})

	return database, err
}
