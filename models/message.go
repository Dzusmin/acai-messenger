package models

type Message struct {
	ID      uint   `json:"id" gorm:"primary_key"`
	Code    string `json:"code"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Email   string `json:"email"`
}
