package main

import (
	"log"
	"os"

	"bitbucket.org/Dzusmin/acai-messenger/command"
	"bitbucket.org/Dzusmin/acai-messenger/controllers"
	"bitbucket.org/Dzusmin/acai-messenger/models"
	"bitbucket.org/Dzusmin/acai-messenger/query"
	"bitbucket.org/Dzusmin/acai-messenger/repository"
	"github.com/gin-gonic/gin"
)

func main() {
	log.Println("Acai messenger started")

	db, err := models.ConnectDataBase()
	if err != nil {
		log.Fatalln(err.Error())
		os.Exit(1)
		return
	}

	messageRepo := repository.NewMessageRepository(db)

	err = setupServer(&messageRepo).Run(":8080")
	if err != nil {
		log.Fatalln(err.Error())
		os.Exit(1)
		return
	}
}

func setupServer(messageRepo *repository.MessageRepository) *gin.Engine {

	amh := command.NewAddMessageHandler(messageRepo)
	dmh := command.NewDeleteMessageHandler(messageRepo)
	gmh := query.NewGetMessagesHandler(messageRepo)
	mc := controllers.NewMessageController(&amh, &dmh, &gmh)

	router := gin.Default()
	router.POST("/api/messages", mc.CreateMessage)
	router.DELETE("/api/messages/:id", mc.DeleteMessage)
	router.GET("/api/messages", mc.GetMessages)

	return router
}
