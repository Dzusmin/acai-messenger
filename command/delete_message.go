package command

import (
	"bitbucket.org/Dzusmin/acai-messenger/errors"
	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type DeleteMessage struct {
	ID   int
	Code string
}

type DeleteMessageHandler struct {
	repo messageRepo
}

type messageRepo interface {
	FindByID(ID int) (*models.Message, error)
	Delete(*models.Message) error
}

func NewDeleteMessageHandler(repo messageRepo) DeleteMessageHandler {
	return DeleteMessageHandler{repo: repo}
}

func (dmh DeleteMessageHandler) Handle(cmd DeleteMessage) error {

	message, err := dmh.repo.FindByID(cmd.ID)

	if err != nil {
		return err
	}

	if message.Code == cmd.Code {
		return dmh.repo.Delete(message)
	}

	return errors.NewCodeIncorrectError("delete code incorrect")
}
