package command

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type FakeDeleteRepo struct {
}

func (FakeDeleteRepo) FindByID(ID int) (*models.Message, error) {
	switch ID {
	case 1:
		return &models.Message{ID: 1, Code: "000000", Title: "title", Content: "content", Email: "email"}, nil
	case 2:
		return &models.Message{ID: 2, Code: "000001", Title: "title", Content: "content", Email: "email"}, nil
	case 3:
		return &models.Message{ID: 3, Code: "999999", Title: "title", Content: "content", Email: "email"}, nil
	default:
		return nil, errors.New("message not found")
	}
}

func (FakeDeleteRepo) Delete(m *models.Message) error {
	if m.ID == 3 {
		return errors.New("message cannot be deleted")
	}

	return nil
}

func TestNewDeleteMessageHandler(t *testing.T) {
	repo := FakeDeleteRepo{}

	handler := NewDeleteMessageHandler(&repo)

	if reflect.TypeOf(handler).Name() != "DeleteMessageHandler" {
		t.Error("Cannot instantiate DeleteMessageHandler with FakeDeleteRepo")
	}
}

func TestDeleteMessageHandle(t *testing.T) {
	repo := FakeDeleteRepo{}

	handler := NewDeleteMessageHandler(&repo)

	var tests = []struct {
		cmd             DeleteMessage
		wantFindError   bool
		wantDeleteError bool
		wantCodeError   bool
	}{
		{
			DeleteMessage{ID: 1, Code: "000000"}, false, false, false,
		}, {
			DeleteMessage{ID: 2, Code: "999999"}, false, false, true,
		}, {
			DeleteMessage{ID: 3, Code: "999999"}, false, true, false,
		}, {
			DeleteMessage{ID: 4, Code: "999999"}, true, false, false,
		},
	}

	for _, tt := range tests {
		testName := fmt.Sprintf("ID: %d, Code: %s", tt.cmd.ID, tt.cmd.Code)
		t.Run(testName, func(t *testing.T) {
			err := handler.Handle(tt.cmd)

			if tt.wantFindError {
				if err.Error() != "message not found" {
					t.Error(fmt.Sprintf("expected not found error, given: %s", err.Error()))
				}
			}

			if tt.wantDeleteError {
				if err.Error() != "message cannot be deleted" {
					t.Error(fmt.Sprintf("expected message cannot be deleted error, given: %s", err.Error()))
				}
			}

			if tt.wantCodeError {
				if err.Error() != "delete code incorrect" {
					t.Error(fmt.Sprintf("expected delete code incorrect error, given: %s", err.Error()))
				}
			}

			if !tt.wantFindError && !tt.wantDeleteError && !tt.wantCodeError {
				if err != nil {
					t.Error(fmt.Sprintf("expected no error, given: %s", err.Error()))
				}
			}
		})
	}
}
