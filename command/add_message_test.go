package command

import (
	"errors"
	"fmt"
	"math/rand"
	"reflect"
	"testing"

	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type FakeAddRepo struct {
	SaveCalled bool
}

func (r *FakeAddRepo) Save(m *models.Message) error {
	r.SaveCalled = true
	if m.Title == "error title" {
		return errors.New("save error")
	}

	m.ID = uint(rand.Intn(100))

	return nil
}

func TestNewAddMessageHandler(t *testing.T) {
	repo := FakeAddRepo{}

	handler := NewAddMessageHandler(&repo)

	if reflect.TypeOf(handler).Name() != "AddMessageHandler" {
		t.Error("Cannot instantiate AddMessageHandler with FakeAddRepo")
	}
}

func TestAddMessageHandle(t *testing.T) {

	var tests = []struct {
		cmd       AddMessage
		wantError bool
	}{
		{
			AddMessage{Title: "example title", Content: "content", Email: "email"}, false,
		}, {
			AddMessage{Title: "error title", Content: "content", Email: "email"}, true,
		},
	}

	for _, tt := range tests {
		testName := fmt.Sprintf("Title: %s, Content: %s, Email: %s", tt.cmd.Title, tt.cmd.Content, tt.cmd.Email)
		t.Run(testName, func(t *testing.T) {
			repo := FakeAddRepo{}
			handler := NewAddMessageHandler(&repo)
			message, err := handler.Handle(tt.cmd)

			if tt.wantError {
				if repo.SaveCalled == false {
					t.Error("Save should be called on repo")
				}
				if err == nil {
					if err == nil {
						t.Error("expected error got nil")
					}
				} else {
					if len(message.Code) != 6 {
						t.Error("expected code to be 6 char long")
					}

					if message.Title != tt.cmd.Title {
						t.Error("expected message to have same title as given to command")
					}

					if message.Content != tt.cmd.Content {
						t.Error("expected message to have same title as given to command")
					}

					if message.Email != tt.cmd.Email {
						t.Error("expected message to have same title as given to command")
					}

					if reflect.TypeOf(message.ID).String() != "uint" {
						t.Error(fmt.Sprintf("Expected uint given %s", reflect.TypeOf(message.ID).String()))
					}
				}
			}
		})
	}

}
