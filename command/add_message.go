package command

import (
	"math/rand"
	"strconv"

	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type AddMessage struct {
	Title   string
	Content string
	Email   string
}

type AddMessageHandler struct {
	repo messagePersister
}

type messagePersister interface {
	Save(*models.Message) error
}

func NewAddMessageHandler(repo messagePersister) AddMessageHandler {
	return AddMessageHandler{repo}
}

func (h AddMessageHandler) Handle(cmd AddMessage) (*models.Message, error) {

	code := generateCode(6)
	messageModel := models.Message{Code: code, Title: cmd.Title, Content: cmd.Content, Email: cmd.Email}

	err := h.repo.Save(&messageModel)

	return &messageModel, err
}

func generateCode(length int) string {
	var code string

	for i := 0; i < length; i++ {
		c := strconv.Itoa(rand.Intn(9))
		code = code + c
	}

	return code
}
