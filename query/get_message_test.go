package query

import (
	"errors"
	"fmt"
	"reflect"
	"testing"

	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type FakeRepo struct {
}

func (FakeRepo) FindByEmailPaged(email string, page, limit int) ([]models.Message, error) {
	if email == "error@mail.co" {
		return nil, errors.New("some simple error")
	}

	var messsages []models.Message

	for i := 0; i < limit; i++ {
		m := models.Message{ID: uint(i), Code: "code", Title: "title", Content: "content", Email: "emal"}
		messsages = append(messsages, m)
	}

	return messsages, nil
}

func TestNewGetMessagesHandler(t *testing.T) {
	repo := FakeRepo{}

	handler := NewGetMessagesHandler(repo)

	if reflect.TypeOf(handler).Name() != "GetMessagesHandler" {
		t.Error("Cannot instantiate GetMessagesHandler with FakeRepo")
	}
}

func TestHandle(t *testing.T) {
	repo := FakeRepo{}

	handler := NewGetMessagesHandler(repo)

	var tests = []struct {
		query         GetMessages
		wantError     bool
		messagesCount int
	}{
		{
			GetMessages{"", 0, 150}, false, 150,
		}, {
			GetMessages{"error@mail.co", 0, 150}, true, 0,
		},
	}

	for _, tt := range tests {
		testName := fmt.Sprintf("email: %s, page: %d, limit: %d", tt.query.Email, tt.query.Page, tt.query.Limit)
		t.Run(testName, func(t *testing.T) {

			messages, err := handler.Handle(tt.query)
			if tt.wantError {
				if err == nil {
					t.Error("expected error got nil")
				}
			} else {
				if len(messages) != tt.messagesCount {

					t.Error(fmt.Sprintf("Expected %d messages, got %d", tt.messagesCount, len(messages)))
				}
			}

		})
	}

}
