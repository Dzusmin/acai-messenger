package query

import (
	"bitbucket.org/Dzusmin/acai-messenger/models"
)

type GetMessages struct {
	Email string
	Page  int
	Limit int
}

type messageGetter interface {
	FindByEmailPaged(email string, page, limit int) ([]models.Message, error)
}

type GetMessagesHandler struct {
	repo messageGetter
}

func NewGetMessagesHandler(repo messageGetter) GetMessagesHandler {
	return GetMessagesHandler{repo: repo}
}

func (handler GetMessagesHandler) Handle(query GetMessages) ([]models.Message, error) {
	return handler.repo.FindByEmailPaged(query.Email, query.Page, query.Limit)
}
