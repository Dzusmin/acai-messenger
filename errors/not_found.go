package errors

type NotFoundError struct {
	s string
}

func NewNotFoundError(s string) error {
	return &NotFoundError{s: s}
}

func (e NotFoundError) Error() string {
	return e.s
}
