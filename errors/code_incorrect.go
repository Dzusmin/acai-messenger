package errors

type CodeIncorrectError struct {
	s string
}

func NewCodeIncorrectError(s string) error {
	return &CodeIncorrectError{s: s}
}

func (e CodeIncorrectError) Error() string {
	return e.s
}
