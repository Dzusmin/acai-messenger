package controllers

import (
	"reflect"
	"testing"

	"bitbucket.org/Dzusmin/acai-messenger/command"
	"bitbucket.org/Dzusmin/acai-messenger/models"
	"bitbucket.org/Dzusmin/acai-messenger/query"
)

type fakeAddHandler struct {
}

func (fakeAddHandler) Handle(cmd command.AddMessage) (*models.Message, error) {
	return nil, nil
}

type fakeDelteHandler struct {
}

func (fakeDelteHandler) Handle(cmd command.DeleteMessage) error {
	return nil
}

type fakeGetHandler struct {
}

func (fakeGetHandler) Handle(query query.GetMessages) ([]models.Message, error) {
	return nil, nil
}

func TestNewMessageController(t *testing.T) {
	fah := &fakeAddHandler{}

	fdh := &fakeDelteHandler{}

	fgh := &fakeGetHandler{}

	mc := NewMessageController(fah, fdh, fgh)

	if reflect.TypeOf(mc).Name() != "MessageController" {
		t.Error("Cannot instantiate MessageController with fakeAddHandler, fakeDeleteHandler, fakeGetHandler")
	}
}

func TestCreateMessage(t *testing.T) {

}
