package controllers

import (
	"net/http"
	"strconv"

	"bitbucket.org/Dzusmin/acai-messenger/command"
	"bitbucket.org/Dzusmin/acai-messenger/models"
	"bitbucket.org/Dzusmin/acai-messenger/query"
	"github.com/gin-gonic/gin"
)

type CreateMessageInput struct {
	Title   string `json:"title" binding:"required"`
	Content string `json:"content" binding:"required"`
	Email   string `json:"author" binding:"required"`
}

type DeleteMessageInput struct {
	Code string `json:"code" binding:"required"`
}

type addHandler interface {
	Handle(cmd command.AddMessage) (*models.Message, error)
}

type deleteHandler interface {
	Handle(cmd command.DeleteMessage) error
}

type getHandler interface {
	Handle(query query.GetMessages) ([]models.Message, error)
}

type MessageController struct {
	AddMessageHandler    addHandler
	DeleteMessageHandler deleteHandler
	GetMessagesHandler   getHandler
}

func NewMessageController(amh addHandler, dmh deleteHandler, gmh getHandler) MessageController {
	return MessageController{amh, dmh, gmh}
}

func (mc MessageController) CreateMessage(c *gin.Context) {

	var input CreateMessageInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cmd := command.AddMessage{Title: input.Title, Content: input.Content, Email: input.Email}
	m, err := mc.AddMessageHandler.Handle(cmd)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": m})
}

func (mc MessageController) DeleteMessage(c *gin.Context) {

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	var input DeleteMessageInput

	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	cmd := command.DeleteMessage{ID: id, Code: input.Code}
	err = mc.DeleteMessageHandler.Handle(cmd)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, "")
}

func (mc MessageController) GetMessages(c *gin.Context) {

	page, err := strconv.Atoi(c.DefaultQuery("page", "0"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	limit, err := strconv.Atoi(c.DefaultQuery("limit", "150"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	if limit > 150 {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Cannot set limit higher then 150"})
		return
	}

	email := c.Query("email")

	query := query.GetMessages{Page: page, Limit: limit, Email: email}
	messages, err := mc.GetMessagesHandler.Handle(query)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": messages})
}
