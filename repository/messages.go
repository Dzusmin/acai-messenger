package repository

import (
	"bitbucket.org/Dzusmin/acai-messenger/errors"
	"bitbucket.org/Dzusmin/acai-messenger/models"
	"gorm.io/gorm"
)

type MessageRepository struct {
	DB *gorm.DB
}

func NewMessageRepository(DB *gorm.DB) MessageRepository {
	return MessageRepository{DB}
}

func (r MessageRepository) FindByID(ID int) (*models.Message, error) {
	var message models.Message

	if err := r.DB.Where("id = ?", ID).First(&message).Error; err != nil {
		return nil, errors.NewNotFoundError("Message not found")
	}

	return &message, nil
}

func (r MessageRepository) Delete(message *models.Message) error {
	return r.DB.Delete(&message).Error
}

func (r MessageRepository) Save(message *models.Message) error {
	return r.DB.Create(message).Error
}

func (r MessageRepository) FindByEmailPaged(email string, page, limit int) ([]models.Message, error) {
	var messages []models.Message
	query := r.DB

	if len(email) > 0 {
		query = r.DB.Where("email = ?", email)
	}

	err := query.Limit(limit).Offset(getOffset(page, limit)).Find(&messages).Error

	return messages, err
}

func getOffset(page, limit int) int {
	return (page - 1) * limit
}
