all: bin/acai-messanger
test: unit-test

PLATFORM=local

export DOCKER_BUILDKIT = 1

.PHONY: bin/acai-messanger
bin/acai-messanger: 
	@docker build . --target bin \
	--output bin/ \
	--platform ${PLATFORM}

.PHONY: up
up:
	@docker-compose up -d 

.PHONY: unit-test
unit-test:
	@docker build . --target unit-test

.PHONY: lint
lint:
	@docker build . --target lint
